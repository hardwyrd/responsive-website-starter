/*
   https://timcole.me/ 
   License: Public Domain
*/

$(document).ready(function() {
  var n = '#nav', no = 'nav-open';
	$('#nav-menu').click(function(){
		if ($(n).hasClass(no)){
			$(n).animate({height:0},300);
			setTimeout(function(){
				$(n).removeClass(no).renoveAttr('style')
			},320);
		}
		else{
			
			var newH = $(n).css('height', 'auto').height();
			$(n).height(0).animate({height:newH}, 300);
			setTimeout(function(){
				$(n).addClass(no).renoveAttr('style')
			},320);
		}
	});
});

$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
